#!/usr/bin/python3
#
# views.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

import sys
import os

from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, HttpResponseRedirect

from .auth import bad_http_method, sync_django_get_user, \
                  sync_login as login, sync_signup as signup, \
                  print_object

from .wss_auth import sync_wss_auth_token as wss_auth_token

from . import settings

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush ()

def wss_auth_demo(request):
    bad=bad_http_method(request, 'GET', 'POST')
    if not (bad is None):
       return bad # wss_auth_demo

    user=sync_django_get_user(request)
    if user is None or \
       not (user.is_authenticated and user.is_active):
       return redirect('login/?next='+request.path) # wss_auth_demo

    #print_object (request)
    return render(request, 'wss_auth_demo.html',
                  {'username': user.username,
                   'is_authenticated': user.is_authenticated
                  }
                 ) # wss_auth_demo
