#!/usr/bin/python3
#
# wss_auth.py
#
# Utility routines for websocket authentication...
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

# defaults if not defined in settings.py...
WSS_AUTH_TOKEN_LENGTH=24
MAX_WSS_AUTH_TIMEOUT_SECONDS=120
MAX_WSS_AUTH_AGE_INTERVAL='2 days' # postgres interval format...
WSS_AUTH_FAIL_LOG=False
MAX_WSS_AUTH_FAIL_LOG_AGE_INTERVAL='3 months' # postgres interval format...

import sys
import os
import inspect
from urllib.parse import urlparse, parse_qs
from socket import gethostbyname
import ipaddress
import inspect
import string
import random
from traceback import format_exc

# If we were doing a PURE asynchronous view,
# we would use a connection pool (future article!)
# The code here will support asynchronous views
# by using a sync_to_async wrapper with this
# SINGLE postgres database connection
# (which is a little less performant...)
from django.db import connection
from django.urls import reverse
from django.http import HttpResponse, JsonResponse
from asgiref.sync import sync_to_async, async_to_sync

# It would be nice to use this on dictionary cursors,
# but this is not implemented for all psycopg versions;
# so we use a simple utility function called
# get_column_indexes in our sql.py and reference all columns
# by tuple index...
#from psycopg.rows import dict_row

UNIT_TEST=__name__=='__main__'
if UNIT_TEST:
   import settings
   import sql
   from auth import bad_http_method

else:
      from . import settings
      from . import sql
      from .auth import bad_http_method

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush () # err

def get_setting(setting, default):
    return getattr(settings, setting, default)

def valid_ip(ip):
    if ip is None:
       return False # valid_ip

    try:
         ipaddress(ip)

    except:
            return False # valid_ip
    
    return True # valid_ip

def get_ip(origin):
    if origin is None:
       return None

    if origin.startswith('https:') or origin.startswith('http:'):
       url=urlparse(origin)
       hostname=url.hostname
       if hostname is None:
          return None # get_ip
   
       if valid_ip(hostname):
          return hostname # get_ip

       return gethostbyname(hostname) # get_ip

    if valid_ip(origin):
       return origin # get_ip

    if origin.startswith('[') and ']' in origin: # remove ip6 brackets...
       close_bracket=origin.find(']')
       origin=origin[1:close_bracket-1]

    colons=origin.count(':')
    if colons in [1, # hostname:port or ip4:port...
                  3, 8 # ip6:port
                 ]: # remove the port suffix...
       colon=origin.rfind(':')
       origin=origin[:colon-1]

    if valid_ip(origin):
       return origin # get_ip

    ip=gethostbyname(origin)
    if valid_ip(ip):
       return ip # get_ip

    err ('get_ip: invalid IP: '+str(origin))
    return None # get_ip

def get_reverse_re_path(name):
    'This fixes up a path returned by the "reverse" function to be suitable for re_path...'
    # A channel URL may or may not include a leading slash
    # (the proxy might remove it?)
    # The regular expression below electively allows it;
    # it will match for either case...

    path=reverse(name) # this will return a NON-regular expression...
    if path.startswith('/'):
       path=path[1:] # remove it...

    # /? -a leading slash is elective...
    return '^/?'+path+'$' # get_reverse_re_path

def get_headers(web_socket):
    bytestring_headers=dict(web_socket.scope['headers'])
    return dict(map(lambda item: (item[0].decode(), item[1].decode()),
                    bytestring_headers.items()
                   )
               ) # get_headers

def get_params(web_socket):
    return dict(parse_qs(web_socket.scope['query_string'].decode('utf-8'))) # get_params

def first_param(params_or_param):
    if isinstance(params_or_param, list):
       return params_or_param[0] # first_param

    return params_or_param # first_param

def get_project(): # get the parent folder of the settings module...
    return os.path.basename(os.path.dirname(os.path.dirname(settings.__file__))) # get_project

def get_app(views): # get the folder of the views module...
    return os.path.basename(os.path.dirname(settings.__file__)) # get_app

def get_parameter(request, parameter_name):
    if request.method=='GET':
       return request.GET.get(parameter_name) # get_parameter

    if request.method=='POST':
       return request.POST.get(parameter_name) # get_parameter

wss_auth_token_length=get_setting('WSS_AUTH_TOKEN_LENGTH',
                                  WSS_AUTH_TOKEN_LENGTH
                                 )
max_wss_auth_timeout_seconds=get_setting('MAX_WSS_AUTH_TIMEOUT_SECONDS',
                                         MAX_WSS_AUTH_TIMEOUT_SECONDS
                                        )
max_wss_auth_age_interval=get_setting('MAX_WSS_AUTH_AGE_INTERVAL',
                                      MAX_WSS_AUTH_AGE_INTERVAL
                                     )
wss_auth_fail_log=get_setting('WSS_AUTH_FAIL_LOG', WSS_AUTH_FAIL_LOG)
max_wss_auth_fail_log_age_interval=get_setting('MAX_WSS_AUTH_FAIL_LOG_AGE_INTERVAL',
                                               MAX_WSS_AUTH_FAIL_LOG_AGE_INTERVAL
                                              )

URL_SAFE_PUNCTUATION="$-_.!*(),"
printable_chars=string.ascii_letters+string.digits+URL_SAFE_PUNCTUATION
printable_chars_length=len(printable_chars)

def get_token(length):
    global printable_chars, printable_chars_length

    return ''.join(random.choices(printable_chars, k=length)) # get_token

def sync_wss_auth_insert(project, app, username, source_ip):
    global wss_auth_token_length, max_wss_auth_age_interval

    token=get_token(wss_auth_token_length)
    try:
         with connection.cursor() as cursor:
              # psycopg3 bug?
              # It appears to get confused when you pass a named parameter
              # to an INTERVAL expression, so we do this instead
              # and use a string .format
              # Since the parameter comes from the settings.py file,
              # which is presumably a trusted input, this is safe to do...
              cursor.execute (sql.WSS_AUTH_DELETE.format(
max_wss_auth_age_interval=max_wss_auth_age_interval
                                                        )
                             ) # cursor.execute
              connection.commit ()
              cursor.execute (sql.WSS_AUTH_INSERT,
                              {'project': project,
                               'app': app,
                               'username': username,
                               'token': token,
                               'ip': source_ip
                              }
                             ) # cursor.execute
              connection.commit ()
    except:
            token=None
            err (format_exc())

    return token # sync_wss_auth_insert

async def async_wss_auth_insert(project, app, username, source_ip):
          return await sync_to_async(sync_wss_auth_insert)(project, app, username, source_ip)

def sync_wss_auth_ok(project, username, token, source_ip):
    global max_wss_auth_timeout_seconds, wss_auth_fail_log, max_wss_auth_fail_log_age_interval

    ok=False
    with connection.cursor() as cursor:
         # psycopg3 bug?
         # It appears to get confused when you pass a named parameter
         # to an INTERVAL expression, so we do this instead
         # and use a string .format
         # Since the parameter comes from the settings.py file,
         # which is presumably a trusted input, this is safe to do...
         sql_select=sql.WSS_AUTH_OK.format(seconds_window=str(max_wss_auth_timeout_seconds)+'s')
         try:
              cursor.execute (sql_select,
                              {'username': username,
                               'token': token
                              }
                             ) # cursor.execute
              ok=cursor.rowcount>0
              if ok:
                 ok=False
                 row=cursor.fetchone()
                 column_index=sql.get_column_indexes(cursor)
                 socket_created_seconds=row[column_index['socket_created_seconds']]
                 if socket_created_seconds is None:
                    cursor.execute (sql.WSS_AUTH_UPDATE,
                                    {'id': row[column_index['id']]}
                                   ) # cursor.execute
                    connection.commit ()
                    ok=cursor.rowcount>0

                 else:
                       err ('wss_auth_ok: username='+str(username)+' '+
                            'attempt to connect to a websocket more than once '+
                            'after it had been opened afer '+str(socket_created_seconds)+' seconds...'
                           )

              else: # not ok
                    err ('wss_auth_ok: username='+str(username)+', '+
                                      'ip='+str(source_ip)+'; '+
                         'websocket token authentication failure...'
                        )
         except:
                  err (format_exc())

         if wss_auth_fail_log and not ok:
            try:
                 # psycopg3 bug?
                 # It appears to get confused when you pass a named parameter
                 # to an INTERVAL expression, so we do this instead
                 # and use a string .format
                 # Since the parameter comes from the settings.py file,
                 # which is presumably a trusted input, this is safe to do...
                 cursor.execute (sql.WSS_AUTH_FAIL_LOG_DELETE.format(
max_wss_auth_fail_log_age_interval=max_wss_auth_fail_log_age_interval
                                                                    )
                                ) # cursor.execute
                 connection.commit ()
                 cursor.execute (sql.WSS_AUTH_FAIL_LOG_INSERT,
                                 {'project': get_project(),
                                  'username': username,
                                  'token': token,
                                  'ip': source_ip
                                 }
                                ) # cursor.execute
                 connection.commit ()

            except:
                    err (format_exc())

    return ok # sync_wss_auth_ok

async def async_wss_auth_ok(web_socket):
          'extracts the parameters for sync_wss_auth_ok from a websocket...'
          project=get_project()
          query_params=get_params(web_socket)
          username=first_param(query_params.get('username', None))
          token=first_param(query_params.get('token', None))
          headers=get_headers(web_socket)
          # If you are using nginx as a proxy front end,
          # for the retrieval of the source_ip to work
          # you will need to add:
          #
          #      proxy_set_header X-Real-IP $remote_addr;
          #
          # by where you set the "proxy_pass" in your nginx config file
          # for your daphne server. If this is not configured, then the source_ip
          # will probably show as 127.0.0.1.
          #
          # If you are running your daphne server without a proxy,
          # then the retrieval of the source ip should work....
          #
          source_ip=headers.get('x-real-ip', None)
          if source_ip is None:
             origin=headers.get('origin', None)
             source_ip=get_ip(origin)

          return await sync_to_async(sync_wss_auth_ok)(project,
                                                       username,
                                                       token,
                                                       source_ip
                                                      ) # async_wss_auth_ok

def sync_wss_auth_token(request):
    'synchronous view function that can be imported to views.py'
    bad=bad_http_method(request, 'GET')
    if not (bad is None):
       return bad # sync_wss_auth_token

    project=get_project()

    this_module=sys.modules[__name__]
    app=get_app(this_module)

    username=get_parameter(request, 'username')
    if username is None or username=='':
       return JsonResponse({'ready': False,
                            'username': None,
                            'token': None,
                            'message': 'User is not authenticated...'
                           }
                          ) # sync_wss_auth_token

    source_ip=request.headers.get('x-real-ip',
                                  request.META.get('REMOTE_ADDR', None)
                                 )
    token=sync_wss_auth_insert(project, app, username, source_ip)
    if token is None:
       return JsonResponse({'ready': False,
                            'username': username,
                            'token': None,
                            'message': 'Failed to insert token; check the Daphne log...'
                           }
                          ) # sync_wss_auth_token

    return JsonResponse({'ready': True,
                         'username': username,
                         'token': token,
                         'message': None
                        }
                       ) # sync_wss_auth_token

async def async_wss_auth_token(request):
          'asynchronous view function that can be imported to views.py'
          bad=bad_http_method(request, 'GET')
          if not (bad is None):
             return bad # async_wss_auth_token

          project=get_project()

          this_module=sys.modules[__name__]
          app=get_app(this_module)

          username=get_parameter(request, 'username')
          if username is None or username=='':
             return JsonResponse({'ready': False,
                                  'username': None,
                                  'token': None,
                                  'message': 'User is not authenticated...'
                                 }
                                ) # async_wss_auth_token

          source_ip=request.headers.get('x-real-ip',
                                        request.META.get('REMOTE_ADDR', None)
                                       )
          token=await async_wss_auth_insert(project, app, username, source_ip)
          if token is None:
             return JsonResponse({'ready': False,
                                  'username': username,
                                  'token': None,
                                  'message': 'Failed to insert token; check the Daphne log...'
                                 }
                                ) # async_wss_auth_token

          return JsonResponse({'ready': True,
                               'username': username,
                               'token': token,
                               'message': None
                              }
                             ) # async_wss_auth_token

if UNIT_TEST:
   TEST_PROJECT='TEST_PROJECT'
   TEST_APP='TEST_APP'
   TEST_USERNAME='TEST_USERNAME'
   TEST_INTERFACE='eno1'

   import socket
   import fcntl
   import struct
   import errno
   import asyncio

   os.environ.setdefault ('DJANGO_SETTINGS_MODULE', 'settings')

   def get_interface_ip(interface_name):
       SIOCGIFADDR=0x8915
       s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
       try:
            ip=socket.inet_ntoa(fcntl.ioctl(s.fileno(),
                                            SIOCGIFADDR,
                                            struct.pack('256s',
                                                        bytes(interface_name[:15],
                                                              'utf-8'
                                                             )
                                                       )
                                           )[20:24]
                               )
       except:
               ip=None

       return ip # get_interface_ip

   test_ip=get_interface_ip(TEST_INTERFACE)
   if test_ip is None: # No such interface;
      test_ip='127.0.0.1' # use this as a backup...

   token=sync_wss_auth_insert('SYNC_'+TEST_PROJECT,
                              'SYNC_'+TEST_APP,
                              'SYNC_'+TEST_USERNAME,
                              test_ip
                             )
   if token is None:
      err ('Unit Test: failed to perform '+
           ("sync_wss_auth_insert('{test_project}', '{test_app}', '{test_username}', '{test_ip}')"
           ).format(test_project='SYNC_'+TEST_PROJECT,
                    test_app='SYNC_'+TEST_APP,
                    test_username='SYNC_'+TEST_USERNAME,
                    test_ip=test_ip
                   )
          )
      sys.exit (errno.ENOMSG)

   print ('passed sync_wss_auth_insert...')

   # try a random token...
   ok=sync_wss_auth_ok('SYNC_'+TEST_PROJECT,
                       'SYNC_'+TEST_USERNAME,
                       get_token(wss_auth_token_length),
                       test_ip
                      )
   if ok: # should fail!
      err ('Unit Test: CATASTROPHIC failure for '+
           ("sync_wss_auth_ok('{test_project}', '{test_username}', '{{token}}', '{test_ip}')"
           ).format(test_project='SYNC_'+TEST_PROJECT,
                    test_username='SYNC_'+TEST_USERNAME,
                    test_ip=test_ip
                   )+'; '+
           'randomly chosen token resulted in access!'
          )
      sys.exit (errno.ENOMSG)

   print ('passed sync_wss_auth_ok (randomly chosen token failed...)')

   # Now try the designated token...
   ok=sync_wss_auth_ok('SYNC_'+TEST_PROJECT,
                       'SYNC_'+TEST_USERNAME,
                       token,
                       test_ip
                      )
   if not ok:
      err ('Unit Test: failed to perform '+
           ("sync_wss_auth_ok('{test_project}', '{test_username}', '{{token}}', '{test_ip}')"
           ).format(test_project='SYNC_'+TEST_PROJECT,
                    test_username='SYNC_'+TEST_USERNAME,
                    test_ip=test_ip
                   )
          )
      sys.exit (errno.ENOMSG)

   print ('passed sync_wss_auth_ok...')

   token=asyncio.run(async_wss_auth_insert('ASYNC_'+TEST_PROJECT,
                                           'ASYNC_'+TEST_APP,
                                           'ASYNC_'+TEST_USERNAME,
                                           test_ip
                                          )
                    )
   if token is None:
      err ('Unit Test: failed to perform '+
           ("async_wss_auth_insert('{test_project}', '{test_app}', '{test_username}', '{test_ip}')"
           ).format(test_project='ASYNC_'+TEST_PROJECT,
                    test_app='ASYNC_'+TEST_APP,
                    test_username='ASYNC_'+TEST_USERNAME,
                    test_ip=test_ip
                   )
          )
      sys.exit (errno.ENOMSG)

   print ('passed async_wss_auth_insert...')
