#!/usr/bin/python3
#
# asgi.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

import sys
import os

os.environ.setdefault ('DJANGO_SETTINGS_MODULE',
                       'simple_sync_wss_auth.settings'
                      )

from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from django.core.asgi import get_asgi_application
from django.urls import path, reverse, re_path

from . import settings
from .consumers import WebSocket
from .wss_auth import get_reverse_re_path

# The following is needed to allow the "reverse" function to work
# at the primitive startup level that this module is loaded at...
import django
django.setup ()

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush ()

socket_routes=URLRouter([
                         # Have the WebSocket live at the same path
                         # as your template page (name='demo'...)
                         re_path(get_reverse_re_path('demo'),
                                 WebSocket.as_asgi()
                                ),

                         # add paths for other WebSocket classes
                         # you want to support here...
                        ]
                       ) # URLRouter
application=ProtocolTypeRouter({'http': get_asgi_application(),
                                'websocket': AuthMiddlewareStack(socket_routes)
                               }
                              ) # ProtocolTypeRouter
