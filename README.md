# **wss_auth - Authentication for Django Channels Using a Database Cluster**

> **NOTE: THIS REPO IS DEPRECATED IN FAVOR OF THE LATEST MEDIUM ARTICLE FOR &quot;**[***Authentication for Django Channels Using a Database Cluster -v3***](https://medium.com/@matt_31770/authentication-for-django-channels-using-a-database-cluster-v3-772befc7faa1)**&quot;; YOU CAN FIND ITS COMPANION REPO HERE:** https://gitlab.com/sourcesolver1/django_wss_auth_v3

##### This is a git assembled for an article on medium.com titled "**Authentication for Django Channels using a Database Cluster**". **For more details, read the article at:**

> https://medium.com/@matt_31770/authentication-for-django-channels-using-a-database-cluster-4800dbfcb07e

##### This git includes a working demo that supports websocket authentication in Django. The code of this git is governed by an MIT license (see LICENSE.txt for details)
---
##### If you want to try a simple single app layout, use the project contained in the folder "simple_sync_wss_auth".

##### If you want to try a simple single app layout that uses asynchronous views, use the project contained in the folder "simple_async_wss_auth".

##### The folder "wss_auth" demonstrates a multi-app project, with one app supporting synchronous views and the other supporting asynchronous ones.

## Demo Installation Checklist

* Extract the project folder to be evaluated to your server (a **sub**folder of **/usr/local** is a good place...)

* Create a superuser for the demo project:

`
 python3 manage.py createsuperuser
`
`
* Edit the .sql files in the "**sql**" subfolder to use the appropriate database roles. These scripts were written for postgres, but can be adapted for the SQL of your choice. Run these scripts for the authentication table(s) to be set up. Note that the tables created by these scripts, **wss_auth** and **wss_auth_fail_log**, do NOT use a project name prefix; rather, these tables will store the project name inside a column of the tables, allowing for centralized queries (as opposed to having to glom together multiple project tables into a single centralized view...)

* Update the database credentials in **settings.py** to use the ones you used when you created the database roles to be used by the .sql files.

## Integration Checklist

If you want to integrate the WebSocket authentication from one of these demo projects into your own project, do the following:

* Edit the .sql files in the "**sql**" subfolder to use the appropriate database roles you are using in your existing project. These scripts were written for postgres, but can be adapted for the SQL of your choice. Run these scripts for the authentication table(s) to be set up.

* Integrate the settings in **settings.py** from the respective project you want to use into your own settings.py file.

* Copy **auth.py**, **wss_auth.py**, and **sql.py** (or integrate it, if you are using one of your own...) into the app subfolder of your project folder.

* In your **views.py**:

    * Import get_script_name, sync_django_login (or async_django_login) as login, sync_django_authenticate (or async_django_authenticate) as authenticate, and sync_authenticator (or async_authenticator) as authenticator from auth.py

    * Import sync_wss_auth_token (or async_wss_auth_token) as wss_auth_token from wss_auth.py

* Incorporate **asgi.py** into your own asgi.py (or add it to your main app subfolder below your project folder, if you don't already have one...) Note that this file module is what is invoked by the interactive Daphne script **daphne.sh**, or your Linux systemd service file **daphne.service**. If you want to set up Daphne as a systemd service, consult the comments in that .service file.

* Integrate **consumers.py** with your own consumers.py (or add it, if you don't have one already...)

* Integrate the JavaScript from the **wss_auth_demo.html** template into your templates, as needed.

* If you are using nginx as a proxy for your Django, integrate the settings in the **nginx.conf.fragment** file into your nginx site file. [**Recommended:** first make a backup of your existing nginx site file] See the comments in this file; they go into the details of using nginx as a proxy for a Daphne server. Do an "nginx -t" to confirm that your edits are correct, and then restart your nginx server (if you are using Linux systemd: "systemctl restart nginx"...)

## Folder Layout
The "wss_auth" project folder shows how a more complex multi-app layout would be accomplished. Typical folder layout for a multi-app Django project:
<pre>
{project_folder_name}/
├── manage.py
    └── templates/
        ├── wss_auth_demo.html
        └── wss_auth.js
├── {project_folder_name}/
│   ├── __init__.py
│   ├── asgi.py
│   ├── auth.py
│   ├── consumers.py
│   ├── settings.py
│   ├── urls.py
│   ├── wsgi.py
│   ├── wss_auth.py
├── {app_folder_name}/ (NOTE: simple projects do not use such a separate subfolder!)
    ├── __init__.py
    ├── migrations/
    │   └── __init__.py
    ├── __init__.py
    ├── urls.py
    └── views.py
├── {app2_folder_name}/ (NOTE: simple projects do not use such a separate subfolder!)
    ├── __init__.py
    ├── urls.py
    └── views.py
 .
 .
 .
</pre>
In many Django projects, the app folder(s) is/are collapsed into a single {project_folder_name}/{project_folder_name} folder; so models.py, urls.py, etc. just live in a single
{project_folder_name}/{project_folder_name} subfolder. This is the layout used in this git for the "simple_..." projects.
