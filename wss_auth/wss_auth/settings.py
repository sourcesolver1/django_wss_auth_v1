#!/usr/bin/python3
#
# settings.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)
#
# For more information on this file, see
# https://docs.djangoproject.com/en/3.1/topics/settings
#
# For the full list of settings and their values, see
# https://docs.djangoproject.com/en/3.1/ref/settings
#

from pathlib import Path
import sys
import os

# Build paths inside the project like this: BASE_DIR / 'subdir'.
this_dir=Path(__file__).resolve().parent
BASE_DIR=Path(__file__).resolve().parent.parent
TEMPLATE_DIR=os.path.join(BASE_DIR, 'templates')

ROOT_URLCONF=os.path.join('async_wss_auth.urls')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG=True

HOST='*' # your FQDN host name, as entered in the browser...
ALLOWED_HOSTS=[HOST, '127.0.0.1']

APPEND_SLASH=True

# Application definition

INSTALLED_APPS=['django.contrib.admin',
                'django.contrib.auth',
                'django.contrib.contenttypes',
                'django.contrib.sessions',
                'django.contrib.messages',
                'django_extensions',
                'daphne',
                'django.contrib.staticfiles',
                'django_countries',
                'async_wss_auth',
                'sync_wss_auth',
                'channels'
               ] # INSTALLED_APPS

CHANNEL_LAYERS={'default': {'BACKEND': 'channels.layers.InMemoryChannelLayer',
                           }
               } # CHANNEL_LAYERS

MIDDLEWARE=['django.middleware.security.SecurityMiddleware',
            'django.middleware.common.CommonMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
            'django.middleware.clickjacking.XFrameOptionsMiddleware',
           ] # MIDDLEWARE

TEMPLATES=[{'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [os.path.join(BASE_DIR, 'templates')],
            'APP_DIRS': True,
            'OPTIONS': {'context_processors': ['django.template.context_processors.debug',
                                               'django.template.context_processors.request',
                                               'django.contrib.auth.context_processors.auth',
                                               'django.contrib.messages.context_processors.messages'
                                              ] # context_processors
                       }
           }
          ] # TEMPLATES

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES={'default': {# Add 'postgresql_psycopg', 'mysql', 'sqlite3' or 'oracle'.
                       'ENGINE': 'django.db.backends.postgresql',

                       # Set to empty string for localhost. Not used with sqlite3...
                       'HOST': '',

                       # Set to empty string for default; Not used with sqlite3...
                       'PORT': '',

                       # DB name or path to database file if using sqlite3...
                       'NAME': 'wss_auth',

                       # Not used with sqlite3;
                       # Replace this with the database account to be used to authenticate,
                       # or create this one and use it...
                       'USER': 'wss_auth',

                       # Not used with sqlite3;
                       'PASSWORD': 'DATABASE_ROLE_THAT_AUTHENTICATES_PASSWORD'
                      } # default
          } # DATABASES

DEFAULT_AUTO_FIELD='django.db.models.AutoField'

SECRET_KEY='%5v2i!#-jd)8z7f*57y98+l#(njq9j*e-0ul07+l+sl6ru#0!y'

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS=[{'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
                          {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
                          {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
                          {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'}
                         ] # AUTH_PASSWORD_VALIDATORS

# Internationalization...
# https://docs.djangoproject.com/en/3.1/topics/i18n
LANGUAGE_CODE='en-us'
TIME_ZONE='UTC'
USE_I18N=True
USE_L10N=True
USE_TZ=True

STATIC_URL='/static/'
STATIC_ROOT=os.path.join(BASE_DIR, 'static')
#STATICFILES_DIRS=(os.path.join(BASE_DIR, 'static'),)

# Absolute filesystem path to the directory that will hold user-uploaded files;
# Example: '/home/media/media.lawrence.com/media/'
TEMPLATES_ROOT=os.path.join(BASE_DIR, 'templates') 

LOGIN_REDIRECT_URL='/login'
LOGOUT_REDIRECT_URL='/logout'

MAX_CREDENTIALS_LENGTH=255 # len('{"username:": "...", "password": "..."}')
WSS_AUTH_CONNECTION_SECONDS=120 # 2 minutes...
MAX_WSS_AUTH_AGE='1 day'
MAX_WSS_AUTH_FAIL_LOG_AGE='3 months'

# If you are using, for example, nginx as a proxy
# this would be the path prefix for which all requests
# get passed on to daphne...
SCRIPT_NAME='wss_auth'

# Use this to log authentication failures to the wss_auth_fail_log table...
#WSS_AUTH_FAIL_LOG=True
