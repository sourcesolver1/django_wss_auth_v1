#!/usr/bin/python3
#
# views.py
#

import sys
import os

from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, HttpResponseRedirect

from wss_auth.auth import bad_http_method, async_django_get_user, \
                          async_login as login, async_signup as signup, \
                          print_object

from wss_auth.wss_auth import async_wss_auth_token as wss_auth_token

from wss_auth import settings

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush ()

async def wss_auth_demo(request):
          bad=bad_http_method(request, 'GET', 'POST')
          if not (bad is None):
             return bad # wss_auth_demo

          user=await async_django_get_user(request)
          if user is None or \
             not (user.is_authenticated and user.is_active):
             return redirect('login/?next='+request.path) # wss_auth_demo
          
          #print_object (request)
          return render(request, 'wss_auth_demo.html',
                        {'username': user.username,
                         'is_authenticated': user.is_authenticated
                        }
                       ) # wss_auth_demo
