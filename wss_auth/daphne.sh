#!/bin/sh
#
# daphne.sh
#
# Interactive test script to run the Daphne server;
# use a Ctrl-C when a test is complete...
#

PORT=8443

/usr/bin/daphne --bind 127.0.0.1 --port=$PORT wss_auth.asgi:application
