-- wss_auth_fail_log.sql
--
-- (c) 2024 Matthew Hopard
-- This code is furnished under an MIT license
-- (see LICENSE.txt for details)
--
-- You will first need to designate roles in postgres
-- for the following template expressions in this script
-- (you can replace the template expressions in this script
--  with the role names you decide on...)
--
-- TRUSTED_OWNER_ROLE: A database user role that is used for setup of sensitive info
--
-- DJANGO_APP_ROLE: The database role used by a Django app needing to log
--                  authentication failures
--
-- ADMINS_GROUP_ROLE: used to grant admin access to the "wss_auth_fail_log_redacted" view
--                    (omits the "token" column...)
--
-- DEVELOPERS_GROUP_ROLE: used to grant developer access to the "wss_auth_fail_log_redacted" view
--                        (omits the "token" column...)
--

CREATE TABLE IF NOT EXISTS wss_auth_fail_log
(id BigSerial PRIMARY KEY NOT NULL,
 date_time Timestamp With Time Zone NOT NULL DEFAULT NOW(),
 project VarChar(255),
 username VarChar(255) NOT NULL,
 token VarChar(255) NOT NULL,
 ip Inet
); -- wss_auth_fail_log

-- If there will be A LOT of records, you can opt to use Timescale
--
--      https://docs.timescale.com/self-hosted/latest
--
-- Pros and Cons:
--
--      https://croz.net/news/timescale-part-two/#:~:text=are%20a%20match.-,Pros%20and%20Cons,-Let%E2%80%99s%20check%20out
--
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_date_time ON wss_auth_fail_log(date_time);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_project ON wss_auth_fail_log(project);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_username ON wss_auth_fail_log(username);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_token ON wss_auth_fail_log(token);
CREATE INDEX IF NOT EXISTS wss_auth_fail_log_ip ON wss_auth_fail_log(ip);

ALTER TABLE wss_auth_fail_log OWNER TO TRUSTED_OWNER_ROLE;
GRANT ALL ON TABLE wss_auth_fail_log TO TRUSTED_OWNER_ROLE;
GRANT INSERT, SELECT, DELETE ON TABLE wss_auth_fail_log TO DJANGO_APP_ROLE;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE wss_auth_fail_log_id_seq TO DJANGO_APP_ROLE;

CREATE OR REPLACE VIEW wss_auth_fail_log_redacted
AS
SELECT id, date_time, project, username, ip
       FROM wss_auth_fail_log;

ALTER VIEW wss_auth_fail_log_redacted OWNER TO TRUSTED_OWNER_ROLE;
GRANT SELECT ON wss_auth_fail_log_redacted TO ADMINS_GROUP_ROLE, DEVELOPERS_GROUP_ROLE;
