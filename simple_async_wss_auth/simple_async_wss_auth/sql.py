#/usr/bin/python3
#
# sql.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

# This can vary from one SQL implementation to another;
# this is the way it is calculated in postgres...
ELAPSED_SECONDS="EXTRACT('EPOCH' FROM {date_time2})-EXTRACT('EPOCH' FROM {date_time1})"

def elapsed_seconds(date_time1, date_time2):
    global ELAPSED_SECONDS

    return ELAPSED_SECONDS.format(date_time1='date_time', date_time2='NOW()') # elapsed_seconds

WSS_AUTH_INSERT='''INSERT INTO wss_auth (project, app, username, token, ip)
VALUES (%(project)s, %(app)s, %(username)s, %(token)s, %(ip)s)
''' # WSS_AUTH_INSERT

# psycopg3 bug? It appears to get confused when you pass a named parameter
# to an INTERVAL expression, so we do this instead and use a string .format
# Since the parameter comes from the settings.py file, which is presumably
# a trusted input, this is safe to do...
WSS_AUTH_OK='''SELECT id, socket_created_seconds
FROM wss_auth
WHERE username=%(username)s AND token=%(token)s AND
      date_time>=NOW()-INTERVAL '{seconds_window}'
ORDER BY date_time DESC
LIMIT 1
''' # WSS_AUTH_OK

WSS_AUTH_UPDATE='''UPDATE wss_auth
SET socket_created_seconds={elapsed_seconds}
WHERE id=%(id)s
'''.format(elapsed_seconds=elapsed_seconds('date_time', 'NOW()')) # WSS_AUTH_UPDATE

# psycopg3 bug? It appears to get confused when you pass a named parameter
# to an INTERVAL expression, so we do this instead and use a string .format
# Since the parameter comes from the settings.py file, which is presumably
# a trusted input, this is safe to do...
WSS_AUTH_DELETE='''DELETE FROM wss_auth
WHERE date_time<NOW()-INTERVAL '{max_wss_auth_age_interval}';
''' # WSS_AUTH_DELETE

WSS_AUTH_FAIL_LOG_INSERT='''INSERT INTO wss_auth_fail_log (project, username, token, ip)
VALUES (%(project)s, %(username)s, %(token)s, %(ip)s)
''' # WSS_AUTH_FAIL_LOG_INSERT

# psycopg3 bug? It appears to get confused when you pass a named parameter
# to an INTERVAL expression, so we do this instead and use a string .format
# Since the parameter comes from the settings.py file, which is presumably
# a trusted input, this is safe to do...
WSS_AUTH_FAIL_LOG_DELETE='''DELETE FROM wss_auth_fail_log
WHERE date_time<NOW()-INTERVAL '{max_wss_auth_fail_log_age_interval}'
''' # WSS_AUTH_FAIL_LOG_DELETE

def get_column_indexes(cursor):
    column_indexes={}
    index=0
    for description in cursor.description:
        column_indexes[description[0]]=index
        index+=1

    return column_indexes # get_column_indexes
