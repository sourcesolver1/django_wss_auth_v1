#!/usr/bin/python3
#
# urls.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

import sys

from django.urls import path, re_path
from django.contrib.auth.views import LogoutView

from . import settings
from . import views

#
# NOTE: set the SCRIPT_NAME setting in your settings.py
# if you are using nginx as a proxy and want to
# redirect all URLs beginning with that path to Daphne.
# Comment the setting out if you are having _all_ URLS sent
# to Daphne and will not be using that prefix path in
# your URLs...
#
script_path='^/?' # /? -handle POSSIBLE leading slash...
script_name=getattr(settings, 'SCRIPT_NAME', '')
if script_name!='':
   script_path+=script_name+'/'

app_name='simple_async_wss_auth'

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush ()

# Since we did an APPEND_SLASH in our settings,
# we use a trailing slash for all our endpoints.
#
# "re_path" is somewhat safer to use than "path"
# because of its precision in being able to use "^"
# to denote the beginning of the path, and its ability
# to handle a missing OR inserted slash which a
# proxy rewrite can unintentionally and unpredictably mangle...
urlpatterns=[
             # These endpoints support a specialized login form;             
             # for demonstration purposes, an account can be made with manage.py
             # or adjust and integrate these routes with your app
             # (after backing up your existing ones!)
             re_path(script_path+'login/$', views.login, name='login'),
             re_path(script_path+'signup/$', views.signup, name='signup'),

             re_path(script_path+'$', views.wss_auth_demo, name='demo'),
             re_path(script_path+'wss_auth_token/$',
                     views.wss_auth_token, name='token'
                    ), # AJAX endpoint...
             re_path(script_path+'logout/$', LogoutView.as_view(), name='logout')
            ] # urlpatterns
