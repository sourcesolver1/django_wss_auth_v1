#!/usr/bin/python3
#
# consumers.py
#
# (c) 2024 Matthew Hopard
# This code is furnished under an MIT license
# (see LICENSE.txt for details)

from channels.generic.websocket import AsyncJsonWebsocketConsumer

import sys
import os
from asyncio import sleep

from .wss_auth import async_wss_auth_ok as wss_auth_ok

def err (message=''):
    sys.stderr.write (str(message)+'\n')
    sys.stderr.flush ()

TEST_MESSAGE='Message {message_number}...'
TEST_MESSAGES=5
TEST_PAUSE_SECONDS=2

class WebSocket(AsyncJsonWebsocketConsumer):
      async def connect (self):
                if await wss_auth_ok(self):
                   await self.accept ()
                   for index0 in range(TEST_MESSAGES):
                       message=TEST_MESSAGE.format(message_number=index0+1)
                       await self.send (message)
                       await sleep (TEST_PAUSE_SECONDS)

                await self.close () # connect
